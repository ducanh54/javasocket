package com.snapsofts.application;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MultiThreadChatServerSync {
	// The server socket
	private static ServerSocket serverSocket = null;
	// The client socket.
	private static Socket clientSocket = null;
	//This chat server can accept up to maxClientsCount clients' connection
	private static final int maxClientsCount = 10;
	private static final ClientThread[] threads = new ClientThread[maxClientsCount];
	
	public static void main(String args[]) {
		
		//The default port number
		int portNumber = 2244;
		if (args.length < 1) {
			System.out.println("Usage: java MultiThreadChatServerSync <portNumber>\n" 
					+ "Now using port number= " + portNumber);
		} else {
			portNumber = Integer.valueOf(args[0]).intValue();
		}
		
		/*
		 * Open a server socket on the portNumber (default 2222). Note that we can
		 * not choose a port less than 1023 if we are not privileged users (root).
		 */
		try {
			serverSocket = new ServerSocket(portNumber);
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*
		 * Create a client socket for each connection and pass it to a
		 * new client thread
		 */
		while (true) {
			try {
				clientSocket = serverSocket.accept();
				int i = 0;
				for (i = 0; i < maxClientsCount; i++) {
					if (threads[i] == null) {
						(threads[i] = new ClientThread(clientSocket, threads)).start();
						break;
					}
				}
				if (i == maxClientsCount) {
					DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
					dataOutputStream.writeUTF("Server too buse. Try later.");
					dataOutputStream.close();
					clientSocket.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
