package com.snapsofts.application;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientThread extends Thread {
	private String clientName = null;
	private DataInputStream dataInputStream = null;
	private DataOutputStream dataOutputStream = null;
	private Socket clientSocket = null;
	private final ClientThread[] threads;
	private int maxClientsCount;
	
	public ClientThread(Socket clientSocket, ClientThread[] threads) {
		this.clientSocket = clientSocket;
		this.threads = threads;
		maxClientsCount = threads.length;
	}
	
	@Override
	public void run() {
		int maxClientCount = this.maxClientsCount;
		ClientThread[] threads = this.threads;
		
		try {
			/*
			 * Create input and output streams for this client
			 */
			dataInputStream = new DataInputStream(clientSocket.getInputStream());
			dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
			String name;
			dataOutputStream.writeUTF("Enter your name.");
			name = dataInputStream.readUTF().trim();
				
			/* Welcome the new the client */
			dataOutputStream.writeUTF("Welcome " + name);
			synchronized (this) {
				for (int i = 0; i < maxClientCount; i++) {
					if (threads[i] != null && threads[i] == this) {
						clientName = "@" + name;
						break;
					}
				}
				for (int i = 0; i <	maxClientCount; i++) {
					if (threads[i] != null && threads[i] != this) {
						threads[i].dataOutputStream.writeUTF("*** A new user " + name + " enteted the chat room !!! ***");
					}
				}
			}
			
			/* Start the conversation. */
			while (true) {
				String line = dataInputStream.readUTF();
				if (line.startsWith("/quit")) {
					break;
				}
				/* If the message is private sent it to the given client */
				if (line.startsWith("@")) {
					String[] words = line.split("\\s", 2);
					if (words.length > 1 && words[1] != null) {
						words[1] = words[1].trim();
						if (!words[1].isEmpty()) {
							synchronized (this) {
								for (int i = 0; i < maxClientCount; i++) {
									if (threads[i] != null && threads[i] != this
											&& threads[i].clientName != null
											&& threads[i].clientName.equals(words[0])) {
										threads[i].dataOutputStream.writeUTF(name + ": " + words[1]);
										/*
										 * Echo this message to let the client know the private
										 * message was sent
										 */
										this.dataOutputStream.writeUTF(name + ": " + words[1]);
										break;
									}
								}
							}
						}
					}
 				} else {
 					/* The message is public, broadcast it to all other clients. */
 					synchronized (this) {
						for (int i = 0; i < maxClientCount; i++) {
							if (threads[i] != null && threads[i].clientName != null) {
								threads[i].dataOutputStream.writeUTF(name + ": " + line);
							}
						}
					}
 				}
			}
			synchronized (this) {
				for (int i = 0; i < maxClientCount; i++) {
					if (threads[i] != null && threads[i] != this
							&& threads[i].clientName != null) {
						threads[i].dataOutputStream.writeUTF("*** The user " + name
								+ " is leaving the chat room !!! ***");
					}
				}
			}
			/*
			 * Clean up. Set the current thread variable to null so that a new client
			 * could be accepted by the server
			 */
			synchronized (this) {
				for (int i = 0; i < maxClientCount; i++) {
					if (threads[i] == this) {
						threads[i] = null;
					}
				}
			}
			/*
			 * Close the output stream, close the input stream, close the soket
			 */
			
			dataInputStream.close();
			dataOutputStream.close();
			clientSocket.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
