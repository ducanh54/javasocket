package com.snapsofts.javasocket02androidclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
	
	private String serverMessage;
	public static String SERVERIP; // your computer IP
	
	public static final int SERVERPORT = 2244;
	private OnMessageReceived mMessageListener = null;
	private boolean mRun = false;
	
	DataOutputStream dataOutputStream;
	DataInputStream dataInputStream;
	
	/**
	 * 
	 * Constructor of the class. OnMessagedReceived listens for the message
	 * received from server
	 * 
	 * @author DucAnhZ
	 *
	 */
	public Client(OnMessageReceived listener) {
		mMessageListener = listener;
	}
	
	public void stopClient() {
		mRun = false;
	}
	
	public void sendMessage(String message) {
		try {
			dataOutputStream.writeUTF(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		mRun = true;
		
		try {
			// here you must put your computer's IP address
			InetAddress serverAddress = InetAddress.getByName(SERVERIP);
			
			// create a socket to make the connection with the server
			Socket socket = new Socket(serverAddress, SERVERPORT);
			
			try {
				// send to message to the server
				dataOutputStream = new DataOutputStream(socket.getOutputStream());
			
				// received the message which the server sends back
				dataInputStream = new DataInputStream(socket.getInputStream());
				
				//in this while the client listens for the messages sent by the server
				while (mRun) {
					serverMessage = dataInputStream.readUTF();
					
					if (serverMessage != null && mMessageListener != null) {
						// call the method messageReceived from MainActivity class
						mMessageListener.messageReceived(serverMessage);
					}
					serverMessage = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				// the socket must be closed. It is not possible to reconnect to
				// this socket
				// after it is closed, which means a new socket instance has to
				// be created
				socket.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	// Declare the interface. The method messageReceived(String message) will
	// must be implemented in the MyActivity
	// class at on asynckTask doInBackground
	public interface OnMessageReceived {
		public void messageReceived(String message);
	}
}
