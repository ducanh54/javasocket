package com.snapsofts.application;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

//Class to manage client chat Box
public class ChatClient {
	/** Chat client access */
	static class ChatAccess extends Observable {
		private Socket socket;
		private DataOutputStream dataOutputStream;
		
		@Override
		public void notifyObservers(Object arg) {
			super.setChanged();
			super.notifyObservers(arg);
		}
		
		/** Create socket, and receiving thread */
		public ChatAccess(String server, int port) throws IOException {
			socket = new Socket(server, port);
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
			
			Thread receivingThread = new Thread() {
				@Override
				public void run() {
					try {
						DataInputStream reader = new DataInputStream(socket.getInputStream());
						String line;
						while ((line = reader.readUTF()) != null) {
							notifyObservers(line);
						}
					} catch (IOException e) {
						notifyObservers(e);
					}
				}
			};
			receivingThread.start();
		}
		
		private static final String CRLF = "\r\n"; // newline
		
		/** Send a line of text */
		public void send(String text) {
			try {
				dataOutputStream.writeUTF(text + CRLF);
			} catch (IOException e) {
				notifyObservers(e);
			}
		}
		
		/** Close the socket */
		public void close() {
			try {
				socket.close();
			} catch (IOException e) {
				notifyObservers(e);
			}
		}
	}
	
	/** Chat client UI */
	 static class ChatFrame extends JFrame implements Observer {
	 	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JTextArea textArea;
        private JTextField inputTextField;
        private JButton sendButton;
        private ChatAccess chatAccess;

        public ChatFrame(ChatAccess chatAccess) {
            this.chatAccess = chatAccess;
            chatAccess.addObserver(this);
            buildGUI();
        }
        
        /** Builds the user interface */
        private void buildGUI() {
            textArea = new JTextArea(20, 50);
            textArea.setEditable(false);
            textArea.setLineWrap(true);
            add(new JScrollPane(textArea), BorderLayout.CENTER);

            Box box = Box.createHorizontalBox();
            add(box, BorderLayout.SOUTH);
            inputTextField = new JTextField();
            sendButton = new JButton("Send");
            box.add(inputTextField);
            box.add(sendButton);

            // Action for the inputTextField and the goButton
            ActionListener sendListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String str = inputTextField.getText();
                    if (str != null && str.trim().length() > 0)
                        chatAccess.send(str);
                    inputTextField.selectAll();
                    inputTextField.requestFocus();
                    inputTextField.setText("");
                }
            };
            inputTextField.addActionListener(sendListener);
            sendButton.addActionListener(sendListener);

            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    chatAccess.close();
                }
            });
        }
        
        /** Updates the UI depending on the Object argument */
        public void update(Observable o, Object arg) {
            final Object finalArg = arg;
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    textArea.append(finalArg.toString());
                    textArea.append("\n");
                }
            });
        }
	 }
	 
	 public static void main(String[] args) {
		 String server = args[0];
		 int port = 2244;
		 ChatAccess access = null;
		 try {
			access = new ChatAccess(server, port);
		} catch (IOException e) {
			 System.out.println("Cannot connect to " + server + ":" + port);
	         e.printStackTrace();
	         System.exit(0);
		}
		 JFrame frame = new ChatFrame(access);
	        frame.setTitle("MyChatApp - connected to " + server + ":" + port);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.pack();
	        frame.setLocationRelativeTo(null);
	        frame.setResizable(false);
	        frame.setVisible(true);
	 }
	
}
