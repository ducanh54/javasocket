package com.snapsofts.application;

import javax.swing.JOptionPane;

public class ChatStart {
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		String IPServer = JOptionPane.showInputDialog("Enter the Server ip adress");
        String[] arguments = new String[] {IPServer};
        	new ChatClient().main(arguments);
	}
}
