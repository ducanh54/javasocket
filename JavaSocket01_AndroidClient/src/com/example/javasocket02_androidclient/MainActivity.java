package com.example.javasocket02_androidclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	TextView textResponse;
	EditText editTextAddress;
	EditText editTextPort;
	Button buttonConnect;
	Button buttonClear;
	EditText welcomeMsg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	   
		editTextAddress = (EditText) findViewById(R.id.address);
		editTextPort = (EditText) findViewById(R.id.port);
		buttonConnect = (Button) findViewById(R.id.connect);
		buttonClear = (Button) findViewById(R.id.clear);
		textResponse = (TextView) findViewById(R.id.response);
		welcomeMsg = (EditText)	findViewById(R.id.welcomemsg);
		
		buttonConnect.setOnClickListener(buttonConnectOnClickListener);
	}
	
	OnClickListener buttonConnectOnClickListener = new OnClickListener() {
		
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public void onClick(View v) {
			
			String tMsg = welcomeMsg.getText().toString();
			if (tMsg.equals("")) {
				tMsg = null;
				Toast.makeText(MainActivity.this, "Null", Toast.LENGTH_SHORT).show();
			}
			MyClientTask myClientTask = new MyClientTask(editTextAddress.getText().toString()
					, Integer.parseInt(editTextPort.getText().toString())
					, tMsg);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				myClientTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				myClientTask.execute();
			}
		}
	};
	
	public class MyClientTask extends AsyncTask<Void, Void, Void> {
		
		String dstAddress;
		int dstPort;
		String response = "";
		String msgToServer;
		
		public MyClientTask(String addr, int port, String msgTo) {
			this.dstAddress = addr;
			this.dstPort = port;
			this.msgToServer = msgTo;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			Socket socket = null;
			DataOutputStream dataOutputStream = null;
			DataInputStream dataInputStream = null;
			try {
				socket = new Socket(dstAddress, dstPort);
				dataOutputStream = new DataOutputStream(socket.getOutputStream());
				dataInputStream = new DataInputStream(socket.getInputStream());
				
				if (msgToServer != null) {
					//Send message
					dataOutputStream.writeUTF(msgToServer);
				}
				
				// If dataInputStream is empty,
				// readUTF() will block the current thread,
				// but not the UI thread (or main thread).
				response = dataInputStream.readUTF();
				
				MainActivity.this.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						Toast.makeText(MainActivity.this, "MyClientTask ended",
								Toast.LENGTH_SHORT).show();
					}
				});
				
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (dataInputStream != null) {
					try {
						dataInputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (dataOutputStream != null) {
					try {
						dataOutputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			textResponse.setText(response);
			super.onPostExecute(result);
		}
	}
	

}
